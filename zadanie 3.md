<div align="center"> <b><h1>Psy </div>
<div align="center"> <b><h6>Oskar Natorski </div>
<div style="page-break-after: always; break-after: page;"></div>

# Slajd nr 1
Pierwsza rzecz, jaka przychodzi nam do głowy na pytanie „Dlaczego kochamy psy” jest taka, że po prostu są z nami bez względu na wszystko. Oto kilka innych powodów, dla których wiemy, że psy dają nam ogromną ilość szczęścia:
> „Bo na tym pieskim świecie to się bardzo liczy, by iść na sześciu łapach i na jednej smyczy” -Marek Majewski.

**„Ktokolwiek wymyślił powiedzenie, że pieniądze nie przynoszą szczęścia, zapomniał o słodkich szczeniaczkach”** - *Gene Hill*
<div style="page-break-after: always; break-after: page;"></div>
<span style="color:red"> Psy na świecie: </span>
# Slajd nr 2

1. Dalmatyńczyk
2. Owczarek niemiecki
3. Jamnik
<span style="color:blue"> Więcej psów na świecie: </span>

* West Terrier
* Dog niemiecki
* Jack Russel
<div style="page-break-after: always; break-after: page;"></div>

# Slajd nr 3
<div align="center"> <b> Typy ras psów </div>
* Rasy duże
    * Rasy średnie
        * Rasy małe
* Rasy bardzo małe  
<div style="page-break-after: always; break-after: page;"></div>

# Slajd nr 4
Wzór na psa

$$ 
\Phi=K_m\int_{0}^{\infty}\frac{d\Phi_{e}(\lambda)}{d\lambda}V(\lambda)d\lambda
$$
<div style="page-break-after: always; break-after: page;"></div>

# Slajd nr 5
```python
Python code:
s = 2
print s
C code:
#include<stdio.h>
int main(void)
{
printf("Hello\n");
return 0;
}
```
<div style="page-break-after: always; break-after: page;"></div>

# Slajd nr 6 
![obrazek](psy-pracujące.jpg)
![obrazek](original.jpg)


